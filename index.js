const express = require("express");

// Mongoose is a package that allows us to create Schemas to model our data structures and to manipulate our databases using different access method
const mongoose = require("mongoose");
const app = express();
const port = 3001;

// [SECTION] MongoDB Connection
/* 
        Syntax:

            mongoose.connect("<MongoDB Atlas Connection String>",
                {
                    // Allows us to avoid any current and future errors while connecting to MongoDB
                    useNewUrlParser: true,
                    useUnifiedTopology: true
                }
            );
*/

mongoose.connect("mongodb+srv://admin:admin@zuitt-bootcamp.bculsyb.mongodb.net/b203_to-do?retryWrites=true&w=majority",
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    });

    let db = mongoose.connection;

    db.on("error", console.error.bind(console, "connection error"));

    db.once("open", () => console.log("We're connected to the cloud database."));




//  [SECTION] Mongoose Schemas
    // Schemas determine the structure of the documents to be written in the database
    // Schemas act as blueprints to our data
/* 
        Syntax:

            const schemaName = new mongoose.Schema({keyvalue:pair});
*/
    // name & status
    // "required" is used to specify that a field must not be empty.
    // "default" is used if a field value is not supplied.

    const taskSchema = new mongoose.Schema({
            name:  {
                type: String,
                required: [true, "Task name is required"]
            }, 
            status: {
                type: String,
                default: "Pending"
            }

    });
/*
                Mini Activity
                    1. Create a User schema.
                        username - string
                        password - string
                    2. Create a User model.
*/

const usersSchema = new mongoose.Schema({
    username:  {
        type: String,
        required: [true, "Username name is required"]
    }, 
    password:{
        type: String,
        required: [true, "Password name is required"]
    } 

});

const Users = mongoose.model("Users", usersSchema);

//  [SECTION] Models
//  Uses schema and use it to create/instantiate documents/object that follows our schema structure.
//  The variable/object that will be create can be used to run commands for interacting with our database.
/* 
        Syntax:

            const VariableName = mongoose.model("collectionName", schemaName);
*/

const Task = mongoose.model("Task", taskSchema);




// middlewares
app.use(express.json()); // Allows app to read json data.
app.use(express.urlencoded({extended:true})); // Allows your app to read data from forms.

// Business Logic
/*
    1. Add a functionality to check if there are duplicate tasks
        - If the task already exists in the database, we return an error message "Duplicate task found"
        - If the task doesn't exist in the database, we add it in the database

        A task exists if:
            - result from the query is not null
            - result.name is equal to req.body.name

    2. The task data will be coming from the request's body

    3. Create a new Task object with a "name" field/property

    4. The "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object
*/

app.post("/tasks", (req, res)=>{

    Task.findOne({name: req.body.name}, (err, result) =>
        {
            if(result !== null && result.name === req.body.name){
                return res.send("Duplicate task found!");
               
            }
            else {
                    // "newTask" was created/instantiaited from the Mongoose schema and will gain access to ".save" method
                let newTask = new Task({
                    name: req.body.name
                });

                newTask.save((saveErr, savedTask) => {
                    if(saveErr){
                        return console.error(saveErr)
                    }
                    else{
                        return res.status(201).send("New Task created!");
                    }
                })
            }
        });
});


app.get("/tasks", (req, res) => {
    Task.find({}, (err, result) => {
        if (err){
            return console.log(err);
        }
        else{
            return res.status(200).send({
                data: result
            });
        }
    });
});


/* Instructions s35 Activity:

1. Using User model.

2. Create a POST route that will access the "/signup" route that will create a user.


5. Process a POST request at the "/signup" route using postman to register a user.

6. Create a git repository named S35.

7. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.

8. Add the link in Boodle. */

app.post("/signup", (req, res)=>{

    Users.findOne({username: req.body.username, password: req.body.password}, (err, result) =>
        {
            if(result !== null && result.username === req.body.username){
                return res.send("Username already taken");
               
            }
            else {
                    // "newTask" was created/instantiaited from the Mongoose schema and will gain access to ".save" method
                let newUser = new Users({
                    username: req.body.username,
                    password: req.body.password
                });

                newUser.save((saveErr, savedUser) => {
                    if(saveErr){
                        return console.error(saveErr)
                    }
                    else{
                        return res.status(201).send("Welcome"+savedUser);
                    }
                })
            }
        });
});

app.get("/user", (req, res) => {
    Users.find({}, (err, result) => {
        if (err){
            return console.log(err);
        }
        else{
            return res.status(200).send({
                data: result
            });
        }
    });
});




app.listen(port, () => console.log(`Server is running at port ${port}`));